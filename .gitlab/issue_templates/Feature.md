## Summary

(Summarize the feature concisely)

## How it works?

(Explain shortly, what this feature work for this extension)

## Expected result

(What you should see/be for this feature)

## Relevant screenshots and/or screen-casts

(Attach link, image or video that relevant for this feature)

## **Details**

**GNOME Shell Version Target:**
*What is gnome shell versions only works with this feature?*

**References:**
*Is there any references about this feature?*

**Configuration category:**
*What is configuration category for this feature; Panel, Dash, Overview, App Grid, or any?*