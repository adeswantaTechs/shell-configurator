# Settings References
You can configure GNOME Shell with these options:

## Panel

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Visibility                            | True          | True/False        | All     | Version 1       |
| Position                              | Top           | Top/Bottom        | All     | Version 1       |
| Auto hide                             | False         | True/False        | N/A     | Version 1       |
| Height                                | 32            | 16 - 128 px       | All     | Version 1       |
| Hot corner **Master**                 | False         | True/False        | All     | Version 5       |

## Dash

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Visibility                            | True          | True/False        | All     | Version 1       |

## Overview

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Workspace switcher visibility         | True          | True/False        | All     | Version 1       |
| Workspace switcher peek width (px)    | -1 (Default)  | -1 - 96           | <= 3.38 | Version 1       |
| Workspace switcher scale size (%)     | 5  (Default)  | 2 - 10            | >= 40   | Version 1       |
| *Search entry visibility*             | True          | True/False        | All     | Version 1       |

## Search **Master**

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Visibility                            | True          | True/False        | N/A     | Version 5       |
| Type to search behavior **Master**    | True          | True/False        | N/A     | Version 5       |

## App Grid

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Row size                              | 4 / 3         | 2 - 12            | All     | Version 1       |
| Column size                           | 6 / 8         | 2 - 12            | All     | Version 1       |

## Looking Glass **Master**

| Option                                | Default       | Value             | Support | Available since |
| ---                                   | ---           | ---               | ---     | ---             |
| Horizontal Position                   | 50            | 50 - 100          | All     | Version 5       |
| Vertical Position                     | Top           | Top/Bottom        | All     | Version 5       |
| Width                                 | 640           | 20 - 2048         | All     | Version 5       |
| Height                                | 480           | 20 - 2048         | All     | Version 5       |

**NOTE:**
* The *Italicized* configuration is obsolete/moved/deleted
* Some options / references maybe outdated/unplanned/untested